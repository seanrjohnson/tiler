#
# Solves a problem from a High School math club competition:
# there is a grid of shapes: triangles, squares or hexagons
# each shape contains a digit or an operator ( + - * / = )
# find all paths through the grid that make a mathematically true
# statement.
#
#	Input: an ascii file where each line contains data for one tile.
#		   [tileNumber]:[tileCharacter]:[numbers of neighboring tiles separated by commas]
#		the first tile is tile 0, the second is tile 1, etc.
#		The tiles must be listed in order without any numbers missing
#		(this is not as robust as it could be, but it works well enough)
#
#	Output: print to stdout a list of all valid equations found in the puzzle
#
#	example: perl tiler.pl a.txt
#	where a.txt is a text file describing a puzzle
#
#
#  The only dependency is perl.  a.txt, b.txt etc. are the puzzle files
#  tiler.pl is the solver. Unfortunately, I've lost the original problem
#  document so I don't have pictures of what the grids actually look like,
#  but it's probably possible to reconstruct them from the puzzle files.
#
#
####################################################
#
#	Sean Johnson, December 27, 2011