use strict;
use warnings;
#
# Solves a problem from a High School math club competition:
# there is a grid of shapes: triangles, squares or hexagons
# each shape contains a digit or an operator ( + - * / = )
# find all paths through the grid that make a mathematically true
# statement.
#
#	Input: an ascii file where each line contains data for one tile.
#		   [tileNumber]:[tileCharacter]:[numbers of neighboring tiles separated by commas]
#		the first tile is tile 0, the second is tile 1, etc.
#		The tiles must be listed in order without any numbers missing
#		(this is not as robust as it could be, but it works well enough)
#
#	Output: print to stdout a list of all valid equations found in the puzzle
#
#	example: perl tiler.pl a.txt
#	where a.txt is a text file describing a puzzle
#
####################################################
#
#	Sean Johnson, December 27, 2011
#

my $infile;
open ($infile, $ARGV[0]);
my @tiles = ();

my $tileIndex = 0;  #read puzzle from input file into a data structure
while (<$infile>) {
	chomp;
	$_ =~ s/^\s*//g; #trim
	$_ =~ s/\s*$//g;
	if ($_ =~ m/^[0-9]+:(([0-9]+)|(\+|-|\*|\/|=)):([0-9]+,*)+$/) { #ignore lines that don't fit this format
		($tiles[$tileIndex]{name},$tiles[$tileIndex]{value},$tiles[$tileIndex]{neighbors}) = split ':';
		$tileIndex = $tileIndex + 1;
	}	
}
close($infile);

print (join("\n",find_eqns(\@tiles)) . "\n"); #solve the puzzle and print the solution

#
#	find_eqns(arrayReference)
#	input: a reference to an array of hashes, each index corresponds to a tile number and contains a hash
#	the hash has fields 'name', 'value', and 'neighbors'
#	'name' and tile index should be identical
#	'value' should be a single character, either a digit or an operator ( + - * / = )
#	'neighbors' is a string composed of names separated by commas
#
#	output: array of strings where each entry is a solution to the puzzle
#
#
sub find_eqns
{
	my $aref = shift;
	my @solns = ();
	my @empty = ();
	foreach my $tile (@$aref)
	{
		push (@solns,@{solve($aref,\@empty,$tile)});
	}
	return @solns;
}

#
#	solve(arrayRef,arrayRef,hashRef)
#
#	Input:	@$tiles: an array containing the whole puzzle (see comments for sub find_eqns)
#			@$usedTiles: an array containing tiles that cannot be traversed (same format as the other array)
#			%$startTile: a hash of the starting tile (same format as one entry into one of the other arrays)
#
#	Output: all solutions to the puzzle where @$usedTiles is the start of the solution and all paths starting
#			from there are tested.
#
#	Note: this uses a recursive algorithm to find all possible solutions
# 
#
sub solve
{
	my ($tiles, $usedTiles, $startTile) = @_;
	my @used = @$usedTiles;
	my @solns = ();
	push (@used, $startTile);
	if (check_soln(\@used)) {
		push(@solns, generate_string(\@used)); 
	}
	my @neighbors = split ',',$startTile->{neighbors};
	foreach my $neighbor (@neighbors) 
	{
		if (not name_in_list($neighbor,\@used))
		{
			push(@solns, @{solve($tiles, \@used, $tiles->[$neighbor])} );
		}
	}
	return \@solns;
}


sub name_in_list
{
	my ($name, $list) = @_;
	my $found = 0;
	foreach my $tile (@$list) 
	{
		if ($name == $tile->{name}) 
		{
			$found = 1;
		}
	}
	return $found;
}

sub check_soln
{
	my $tiles = shift;
	my $string = '';
	my $result = 0;
	foreach my $tile (@$tiles)
	{
		$string = $string . $tile->{value};
	}
	$string =~ s/\s//; #remove whitespace
	if ($string =~ /^[0-9]+((\+|-|\*|\/)[0-9]+)*=[0-9]+((\+|-|\*|\/)[0-9]+)*$/) #true if it is a properly formatted equation
	{
		$string =~ s/=/==/g; #so it is a valid perl expression
		$result = eval $string; #evaluate the string as a perl program
	}
	return $result;
}

sub generate_string 
{
	my $tiles = shift;
	my $string = '';
	foreach my $tile (@$tiles)
	{
		$string = $string . $tile->{value} . " ";
	}
	return $string;
}